import React, { Component } from 'react';

export default class ErrorDisplay extends Component {
    render() {
        return (
            <div style={{ color:"red" }} className="error-display">
                {
                    this.props.errors.map((error, index) => 
                        <p key={index}>{error}</p>
                    )
                }
            </div>
        )
    }

}