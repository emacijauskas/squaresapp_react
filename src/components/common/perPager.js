import React, { Component } from 'react';

export default class PerPager extends Component {
    render() {
        let changeItemsPerPageCount = this.props.changeCountPerPage.bind(this, this.props.perPage);

        return (
            <a onClick={changeItemsPerPageCount}>{this.props.perPage}</a>
        )
    }
}