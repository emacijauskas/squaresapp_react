import React, { Component } from 'react';
import { Button } from 'react-bootstrap';

import PointsRepo from './../pointsRepo';

export default class List extends Component {
    constructor(props) {
        super(props);

        this.removeList = this.removeList.bind(this);
    }
    
    removeList() {
        PointsRepo
            .removeList(this.props.name)
                .then(() => {
                    this.props.resetToDefaultList();
                    this.props.reloadLists();
                    window.dispatchEvent(new CustomEvent("changeList"));
                });
    }

    render() {
        const st = {display: "inline-block"};
        const st2 = {marginRight: 10};
        return (
            <div style={st}>
                <Button active={this.props.isActive} value={this.props.name} onClick={this.props.loadPointsFromList}>{this.props.name}</Button>
                <a style={st2} onClick={this.removeList}>D</a>
            </div>
        )
    }
}