import React, { Component } from 'react';
import { Button, Row } from 'react-bootstrap';

import PointsRepo from './../pointsRepo';
import NewListForm from './newListForm';
import List from './list';

export default class Lists extends Component {
    constructor(props) {
        super(props);

        this.state = {
            lists: [],
            currentList: ""
        }

        this.loadPointsFromList = this.loadPointsFromList.bind(this);
        this.loadLists = this.loadLists.bind(this);
        this.resetToDefaultList = this.resetToDefaultList.bind(this);
    }

    componentDidMount() {
        this.loadLists();
    }

    loadLists() {
        PointsRepo
            .getLists()
                .then(resp => this.setState({ lists: resp }));
    }

    loadPointsFromList(event) {
        this.setState({ currentList: event.target.value });
        window.dispatchEvent(new CustomEvent('changeList', { detail: event.target.value }));
    }

    resetToDefaultList() {
        this.setState({ currentList: "" });
    }

    render() {
        const customListsSt = {marginTop: 10};
        return (
            <Row>
                <h4>Existing lists</h4>
                    <div>
                        <Button active={this.state.currentList === ""} value={""} onClick={this.loadPointsFromList}>Default</Button>
                        <div style={customListsSt}>
                            {                            
                                this.state.lists.length > 0
                                    ? <span>Custom: </span>
                                    : null
                            }
                            {
                                this.state.lists.map((listName, index) => 
                                    <List 
                                        resetToDefaultList={this.resetToDefaultList}
                                        loadPointsFromList={this.loadPointsFromList} 
                                        reloadLists={this.loadLists} 
                                        isActive={this.state.currentList === listName} 
                                        key={index} 
                                        name={listName} />
                                )
                            }
                        </div>
                    </div>
                    {
                        this.state.currentList === ""
                            ? <NewListForm  reloadLists={this.loadLists} />
                            : null
                    }
            </Row>
        )
    }
}