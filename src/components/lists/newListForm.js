import React, { Component } from 'react';
import { Button, FormGroup, ControlLabel, FormControl } from 'react-bootstrap';

import PointsRepo from './../pointsRepo';

export default class NewListForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            newListName: ""
        }

        this.handleNewListNameInput = this.handleNewListNameInput.bind(this);
        this.submitNewList = this.submitNewList.bind(this);
    }

    handleNewListNameInput(event) {
        this.setState({ newListName: event.target.value });
    }

    submitNewList(event) {
        event.preventDefault();

        PointsRepo
            .saveList(this.state.newListName)
                .then(() => {
                    this.props.reloadLists();
                    this.setState({ newListName: "" });

                    window.dispatchEvent(new CustomEvent("changeList"));
                });
    }

    render() {
        return (
            <div>
                <form onSubmit={this.submitNewList}>

                    <FormGroup controlId="new-list-name">
                        <ControlLabel>
                            New list name:
                        </ControlLabel>
                        <FormControl 
                            type="text"
                            value={this.state.newListName}
                            onChange={this.handleNewListNameInput}>
                        </FormControl>
                    </FormGroup>

                    <Button
                        type="submit"
                        bsStyle="primary"
                        bsSize="large"
                        disabled={this.state.newListName === ""}
                        className="submit-new-list-button">Submit
                    </Button>

                </form>
            </div>
        )
    }
}