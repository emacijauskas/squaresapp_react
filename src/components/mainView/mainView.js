import React, { Component } from 'react';
import { Grid, Row, Col } from 'react-bootstrap';

import Plane from './../plane/plane';
import NewPointForm from './../point/newPointForm';
import Uploader from './../uploader/uploader';
import SquaresList from './../squares/squaresList';
import Lists from './../lists/lists';

export default class MainView extends Component {
    constructor(props) {
        super(props);

        this.state = {
            listName: ""
        }
    }

    componentDidMount() {
        var self = this;
        
        window.addEventListener("changeList", function(event) {
            var listName = '';
            if(event.detail)
            {
                listName = event.detail;
            }

            self.setState({ listName: listName }, () => {
                window.dispatchEvent(new CustomEvent('reloadPoints'));
                window.dispatchEvent(new CustomEvent('loadSquares'));
            });
        });
    }

    render() {
        const rowMargin = {marginTop:30}

        let currentListName;
        if(this.state.listName === "") {
            currentListName = "default";
        } else {
            currentListName = this.state.listName;
        }

        return (
            <div>
                <Grid>
                    <h2>Current list: <b>{currentListName}</b></h2>
                    <Row style={rowMargin}>
                        <Col xs={4}>
                            <NewPointForm currentListName={this.state.listName} />
                            <Uploader currentListName={this.state.listName} />
                        </Col>
                        <Col xs={4}>
                            <Lists />
                        </Col>
                    </Row>
                    <Row style={rowMargin}>
                        <Col xs={6}>
                            <Plane currentListName={this.state.listName} />
                        </Col>
                        <Col xs={6}>
                            <SquaresList currentListName={this.state.listName} />
                        </Col>
                    </Row>
                </Grid>
                
            </div>
        )
    }
}