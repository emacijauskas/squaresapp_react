import React, { Component } from 'react';
import ReactPaginate from 'react-paginate';

import Point from './../point/point';
import PointsRepo from './../pointsRepo';
import PerPager from './../common/perPager';

export default class Plane extends Component {
    constructor(props) {
        super(props);

        this.state = {
            coordinates: [],
            itemCount: 0,
            pageCount: 0,
            perPage: 5,
            page: 0
        }

        this.handlePageClick = this.handlePageClick.bind(this);
        this.loadPoints = this.loadPoints.bind(this);
        this.clearPoints = this.clearPoints.bind(this);
        this.changeCountPerPage = this.changeCountPerPage.bind(this);
    }

    componentDidMount() {
        var self = this;
        window.addEventListener('reloadPoints', function(event) {
            self.loadPoints();
        });

        this.loadPoints();
    }

    handlePageClick(data) {
        this.setState({ page: data.selected }, () => this.loadPoints());
    }

    clearPoints(event) {
        event.preventDefault();

        PointsRepo
            .clearPoints(this.props.currentListName)
            .then(() => {
                this.loadPoints();
                window.dispatchEvent(new CustomEvent("loadSquares"));
            });
    }

    loadPoints() {
        PointsRepo
            .getPoints(this.state.page, this.state.perPage, this.props.currentListName)
            .then(points => 
                this.setState({
                    coordinates: points.points,
                    pageCount: Math.ceil(points.totalPoints / this.state.perPage),
                    itemCount: points.totalPoints
                })
            );
    }

    changeCountPerPage(val) {
        if(val !== this.state.perPage)
        {
            this.setState({ 
                perPage: val,
                page: 0
            }, () => this.loadPoints());
        }
    }

    render() {
        const paginSt = {margin: '0 auto', color: 'red'};

        return (
            <div className="plane">
                <h3>List of points ({this.state.itemCount})</h3>
                <div>
                    {   
                        this.state.coordinates.length > 0
                            ? 
                                this.state.coordinates.map((point, index) =>
                                    <Point currentListName={this.props.currentListName} reloadPoints={this.loadPoints} key={index} coordinates={point} />
                                )
                            :
                                <p>(no points)</p>
                    }
                </div>

                <ReactPaginate
                    style={paginSt}
                    previousLabel={"previous"}
                    nextLabel={"next"}
                    breakLabel={<a href="">...</a>}
                    breakClassName={"break-me"}
                    pageCount={this.state.pageCount}
                    marginPagesDisplayed={2}
                    pageRangeDisplayed={5}
                    onPageChange={this.handlePageClick}
                    containerClassName={"pagination"}
                    subContainerClassName={"pages pagination"}
                    activeClassName={"active"} />

                <div>
                    <span>Per page: </span>
                    <PerPager perPage={5} changeCountPerPage={this.changeCountPerPage} />&nbsp;
                    <PerPager perPage={10} changeCountPerPage={this.changeCountPerPage} />&nbsp;
                    <PerPager perPage={20} changeCountPerPage={this.changeCountPerPage} />&nbsp;
                    <PerPager perPage={50} changeCountPerPage={this.changeCountPerPage} />&nbsp;
                </div>
                
                <div>
                    {
                        this.state.itemCount > 0
                            ? <a className="clear-list-button" onClick={this.clearPoints}>Clear points</a>
                            : null
                    }
                </div>

                <div>
                    {
                        this.state.itemCount > 0
                            ? <a href={PointsRepo.pointsDownloadLink(this.props.currentListName)} target="_blank">Download list</a>
                            : null
                    }
                </div>
                
            </div>
        )
    }
}