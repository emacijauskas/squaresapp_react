import React, { Component } from 'react';
import { Button, FormGroup, FormControl, ControlLabel } from 'react-bootstrap';

import PointsRepo from './../pointsRepo';
import ErrorDisplay from './../common/errorDisplay';

export default class NewPointForm extends Component {
    constructor(props) {
        super(props)

        this.state = {
            x: "",
            y: "",
            errors: []
        }

        this.handleInputX = this.handleInputX.bind(this);
        this.handleInputY = this.handleInputY.bind(this);
        this.submitNewPoint = this.submitNewPoint.bind(this);
    }

    submitNewPoint(event) {
        event.preventDefault();

        PointsRepo.submitNewPoint(this.state.x, this.state.y, this.props.currentListName)
            .then(result => {
                this.setState({ 
                    errors: result.meta.errors ,
                    x: "",
                    y: ""
                });
                if(result.meta.hasSucceeded)
                {
                    window.dispatchEvent(new CustomEvent('reloadPoints'));
                    window.dispatchEvent(new CustomEvent('loadSquares'));
                }
            });
    }

    handleInputX(event) {
        this.setState({ x: event.target.value })
    }

    handleInputY(event) {
        this.setState({ y: event.target.value })
    }

    render() {
        return (
            <div className="new-point-form">

                <h4>Add new point (Range (-5000) - (5000)):</h4>
                <form onSubmit={this.submitNewPoint}>

                    <FormGroup controlId="newPointXName">
                        <ControlLabel>
                            X
                        </ControlLabel>
                        <FormControl 
                            type="number"
                            value={this.state.x}
                            onChange={this.handleInputX}>
                        </FormControl>
                    </FormGroup>

                    <FormGroup controlId="newPointYName">
                        <ControlLabel>
                            Y
                        </ControlLabel>
                        <FormControl 
                            type="number"
                            value={this.state.y}
                            onChange={this.handleInputY}>
                        </FormControl>
                    </FormGroup>

                    <Button
                        type="submit"
                        bsStyle="primary"
                        bsSize="large"
                        disabled={this.state.x === "" || this.state.y === ""}
                        className="submit-new-point-button">Submit
                    </Button>

                    <ErrorDisplay errors={this.state.errors} />
                </form>
            </div>
        )
    }
}