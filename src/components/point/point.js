import React, { Component } from 'react';

import PointsRepo from './../pointsRepo';

export default class Point extends Component {
    constructor(props) {
        super(props);

        this.state = {
            errors: []
        }

        this.removePoint = this.removePoint.bind(this);
    }

    removePoint() {
        PointsRepo
            .removePoint(this.props.coordinates[0], this.props.coordinates[1], this.props.currentListName)
                .then(result => {
                    if(result.meta.hasSucceeded)
                    {
                        this.props.reloadPoints();
                        window.dispatchEvent(new CustomEvent("loadSquares"));
                    }
                });
    }

    render() {
        return (
            <span>[{this.props.coordinates[0]}; {this.props.coordinates[1]}]<a onClick={this.removePoint}>D</a>&nbsp;</span>
        )
    }
}