const apiEndpoint = "http://localhost:52839";

var pointsRepo = {
    getPoints: function(page, perPage, listName) {
        return fetch(`${apiEndpoint}/api/square/getplane?page=${page}&perpage=${perPage}&listname=${listName}`)
            .then(response => response.json())
            .catch(error => console.log(error))
    },

    submitNewPoint: function(x, y, listName) {
        return fetch(`${apiEndpoint}/api/square/addpoint`, {
            method: 'POST',
            headers: { 'Content-Type': 'application/json', 'Accept': 'application/json' },
            body: JSON.stringify({ x: x, y: y, listName: listName})
        })
        .then(response => response.json())
        .catch(error => console.log(error))
    },

    clearPoints: function(listName) {
        return fetch(`${apiEndpoint}/api/square/clearplane?listname=${listName}`, {
            method: 'PUT'
        })
            .then(response => response.json())
            .catch(error => console.log(error))
    },

    removePoint: function(x, y, listName) {
        return fetch(`${apiEndpoint}/api/square/removepoint?listname=${listName}`, {
            method: 'DELETE',
            headers: { 'Content-Type': 'application/json', 'Accept': 'application/json' },
            body: JSON.stringify({ x: x, y: y, listName: listName})
        })
        .then(response => response.json())
        .catch(error => console.log(error))
    },

    uploadFile: function(formData, listName) {
        return fetch(`${apiEndpoint}/api/square/upload?listname=${listName}`, {
            method: 'POST',
            body: formData
        })
        .then(response => response.json())
        .catch(error => console.log(error))
    },

    getSquares: function(page, perPage, listName) {
        return fetch(`${apiEndpoint}/api/square/getsquares?page=${page}&perpage=${perPage}&listname=${listName}`)
            .then(resp => resp.json())
            .catch(err => console.log(err))
    },

    getLists: function() {
        return fetch(`${apiEndpoint}/api/square/getlists`)
            .then(resp => resp.json())
            .catch(err => console.log(err))
    },

    saveList: function(name) {
        return fetch(`${apiEndpoint}/api/square/savelist?name=${name}`, {
            method: 'PUT'
        })
            .then(resp => resp.json())
            .catch(err => console.log(err))
    },

    removeList: function(name) {
        return fetch(`${apiEndpoint}/api/square/deletelist?name=${name}`, {
            method: 'DELETE'
        })
            .then(resp => resp.json())
            .catch(err => console.log(err))
    },

    pointsDownloadLink: function(listName) {
        return `${apiEndpoint}/api/square/download?listname=${listName}`
    }
}

export { pointsRepo as default }