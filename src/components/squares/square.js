import React, { Component } from 'react';

export default class Square extends Component {
    render() {
        return (
            <div className="square-corners">
                {
                    this.props.corners.map((corner, index) => 
                        <span key={index}>[{corner[0]}; {corner[1]}]</span>
                    )
                }
            </div>
        )
    }
}