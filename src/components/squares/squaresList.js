import React, { Component } from 'react';
import ReactPaginate from 'react-paginate';

import Square from './square';
import PointsRepo from './../pointsRepo';
import PerPager from './../common/perPager';

export default class SquaresList extends Component {
    constructor(props) {
        super(props);

        this.state = {
            squares: [],
            itemCount: 0,
            pageCount: 0,
            perPage: 5,
            page: 0            
        };

        this.loadSquares = this.loadSquares.bind(this);
        this.handlePageClick = this.handlePageClick.bind(this);
        this.changeCountPerPage = this.changeCountPerPage.bind(this);
    }

    componentDidMount() {
        var loadSquares = this.loadSquares;
        window.addEventListener('loadSquares', function() {
            loadSquares();
        });

        this.loadSquares();
    }

    handlePageClick(data) {
        this.setState({ page: data.selected }, () => this.loadSquares());
    }

    changeCountPerPage(val) {
        if(val !== this.state.perPage)
        {
            this.setState({ 
                perPage: val, 
                page: 0 
            }, () => this.loadSquares());
        }
    }

    loadSquares() {
        PointsRepo
            .getSquares(this.state.page, this.state.perPage, this.props.currentListName)
                .then(response => this.setState({ 
                    squares: response.squares,
                    itemCount: response.totalSquares,
                    pageCount: Math.ceil(response.totalSquares / this.state.perPage)
                }));
    }
    
    render() {
        return (
            <div className="squares">
                <h3>Squares ({this.state.itemCount})</h3>
                <div className="squares-list">
                    {
                        this.state.squares.length > 0
                            ?
                                this.state.squares.map((square, index) => 
                                    <Square corners={square} key={index} />
                                )
                            :
                                <p>(no squares)</p>
                    }
                </div>

                <ReactPaginate 
                    previousLabel={"previous"}
                    nextLabel={"next"}
                    breakLabel={<a href="">...</a>}
                    breakClassName={"break-me"}
                    pageCount={this.state.pageCount}
                    marginPagesDisplayed={2}
                    pageRangeDisplayed={5}
                    onPageChange={this.handlePageClick}
                    containerClassName={"pagination"}
                    subContainerClassName={"pages pagination"}
                    activeClassName={"active"} />

                    <div>
                        <span>Per page: </span>
                        <PerPager perPage={5} changeCountPerPage={this.changeCountPerPage} />&nbsp;
                        <PerPager perPage={10} changeCountPerPage={this.changeCountPerPage} />&nbsp;
                        <PerPager perPage={20} changeCountPerPage={this.changeCountPerPage} />&nbsp;
                        <PerPager perPage={50} changeCountPerPage={this.changeCountPerPage} />&nbsp;
                    </div>

            </div>
        )
    }
}