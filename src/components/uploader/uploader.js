import React, { Component } from 'react';
import FileInput from 'react-file-input';

import PointsRepo from './../pointsRepo';
import ErrorDisplay from './../common/errorDisplay';

export default class Uploader extends Component {
    constructor(props) {
        super(props);

        this.state = {
            errors: []
        }

        this.handleFileInput = this.handleFileInput.bind(this);
    }

    handleFileInput(event) {
        var fd = new FormData();
        fd.append('data', event.target.files[0]);
        PointsRepo
            .uploadFile(fd, this.props.currentListName)
            .then(res => {
                this.setState({ errors: res.meta.errors })
                if(res.meta.hasSucceeded)
                {
                    window.dispatchEvent(new CustomEvent('reloadPoints'));
                    window.dispatchEvent(new CustomEvent('loadSquares'));
                }
            });
    }

    render() {
        const uploaderSt = {float: "right"};
        return (
            <div style={uploaderSt}>
                <form>
                    <FileInput name="pointsUploader"
                    accept=".txt"
                    placeholder="Upload points file"
                    className="inputClass"
                    onChange={this.handleFileInput} />
                </form>
                <ErrorDisplay errors={this.state.errors} />
            </div>
        )
    }
}