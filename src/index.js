import React from 'react';
import ReactDOM from 'react-dom';

import MainView from './components/mainView/mainView';

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap-theme.css';

ReactDOM.render(
  <MainView />,
  document.getElementById('root')
);
